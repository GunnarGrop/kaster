* Kaster
  #+CAPTION: License shield
  #+NAME: fig:license-shield
  [[https://img.shields.io/badge/license-GPL--3.0--or--later-informational.svg]]
  \\
  #+CAPTION: Preview image of the application
  #+NAME: fig:preview
  [[./preview.png]]
  \\
  A podcast player written in C++, Qt and Kirigami.
  Very much a work in progress, but the player is capable of adding podcast subscriptions via RSS feeds, downloading episodes and playing them.
** Build from source
*** Dependencies
    OpenSUSE Tumebleweed
    #+BEGIN_SRC conf
    zypper in -t pattern devel_kde_frameworks
    zypper in git cmake gcc-c++ libQt5QuickControls2-devel
    #+END_SRC conf

    Ubuntu 20.10
    #+BEGIN_SRC conf
    apt install git build-essential g++ cmake extra-cmake-modules qtbase5-dev qtdeclarative5-dev qtmultimedia5-dev qtquickcontrols2-5-dev kirigami2-dev libkf5i18n-dev libkf5coreaddons-dev libkf5widgetsaddons-dev gettext libqt5multimedia5-plugins
    #+END_SRC conf

*** Building and running
    #+BEGIN_SRC conf
    git clone https://gitlab.com/GunnarGrop/kaster.git
    cd kaster
    mkdir build && cd build
    cmake ..
    make
    ./src/Kaster
    #+END_SRC conf
