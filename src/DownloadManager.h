/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DOWNLOAD_MANAGER_H
#define DOWNLOAD_MANAGER_H

#include <QtNetwork>

class DownloadManager : public QObject {
    Q_OBJECT

private:
    QNetworkAccessManager* _manager;

public:
    explicit DownloadManager(QObject *parent = nullptr);
    QNetworkReply* get(QUrl url);

public slots:
    void encrypted();
    void errorOccurred(QNetworkReply::NetworkError code);
    void finished();
    void redirected(const QUrl &url);
    void sslErrors(const QList<QSslError> &errors);

};

#endif // DOWNLOAD_MANAGER_H
