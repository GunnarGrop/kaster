/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "DownloadManager.h"

DownloadManager::DownloadManager(QObject *parent) : QObject(parent) {
    _manager = new QNetworkAccessManager(this);
}

QNetworkReply* DownloadManager::get(QUrl url) {
    QNetworkRequest request;
    request.setUrl(url);
    request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, true);

    QNetworkReply *reply = _manager->get(request);
    connect(reply, SIGNAL(encrypted()), this, SLOT(encrypted()));
    connect(reply, SIGNAL(errorOccurred(QNetworkReply::NetworkError)),
	    this, SLOT(errorOccurred(QNetworkReply::NetworkError)));

    connect(reply, SIGNAL(finished()), this, SLOT(finished()));
    connect(reply, SIGNAL(redirected(const QUrl)), this, SLOT(redirected(const QUrl)));
    connect(reply, SIGNAL(sslErrors(const QList<QSslError>)),
	    this, SLOT(sslErrors(const QList<QSslError>)));

    qDebug() << "Download started: " << reply->url();
    return reply;
}

// SLOTS
void DownloadManager::encrypted() {
    qDebug() << "SSL/TLS session has successfully completed the initial handshake: " << qobject_cast<QNetworkReply*>(sender())->url();
}

void DownloadManager::errorOccurred(QNetworkReply::NetworkError code) {
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    qDebug() << "Network error:" << code << reply->errorString();
    reply->deleteLater();
}

void DownloadManager::finished() {
    qDebug() << "Download request finished: " << qobject_cast<QNetworkReply*>(sender())->url();
}

void DownloadManager::redirected(const QUrl &url) {
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    qDebug() << "HTTP redirect: " << reply->url() << " -> " << url.toString();
}

void DownloadManager::sslErrors(const QList<QSslError> &errors) {
    qDebug() << "SSL error: ";
    for (QSslError error : errors) {
	qDebug() << error;
    }
    qobject_cast<QNetworkReply*>(sender())->ignoreSslErrors();
}
