/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DATABASE_MANAGAER_H
#define DATABASE_MANAGAER_H

#include <QObject>
#include <QtSql>
#include <QVariant>
#include <QDebug>
#include "PodcastModel.h"

class DatabaseManager : public QObject {
    Q_OBJECT

private:
    QString _db_path;
    QString _db_name;
    QSqlDatabase _db;

public:
    explicit DatabaseManager(QObject *parent = nullptr);

    qint64 selectPodcastCount();
    qint64 selectEpisodeCount();
    qint64 selectLatestPodcastId();
    qint64 selectLatestEpisodeId();
    qint64 selectPodcastIdByRss(QString rssUrl);
    QList<QObject*> selectAllPodcasts();
    QList<QObject*> selectAllEpisodes();
    void insertNewPodcast(PodcastModel *newPodcast);
    void insertNewEpisode(EpisodeModel *newEpisode);
    void insertNewEpisodeList(QList<QObject*> episodeList);
    void updateFilePath(qint64 id, QString filePath);
    void updatePlayed(qint64 id, bool state);
    void deletePodcast(qint64 podcastId);
    void updatePlayedDuration(qint64 id, qint64 playedDuration);

public slots:
    void updateFilePath(QString filePath);
    void updatePodcastImageUrl(QString imagePath);
};


#endif // DATABASE_MANAGAER_H
