/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "DatabaseManager.h"

DatabaseManager::DatabaseManager(QObject *parent) : QObject(parent) {
    _db_path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    _db_name = "podcast.db";
    _db = QSqlDatabase::addDatabase("QSQLITE");

    // Create a default database if one doesn't already exists
    QDir dir(_db_path);
    QFile file(_db_path + "/" + _db_name);
    if (!file.exists()) {
	if (!dir.exists()) {
	    dir.mkpath(_db_path);
	}

	_db.setDatabaseName(_db_path + "/" + _db_name);
	_db.open();
	QSqlQuery query;
        query.exec("create table podcast "
		   "(podcastId INTEGER PRIMARY KEY, "
		   "title TEXT, "
		   "description TEXT, "
		   "rssUrl TEXT, "
		   "imageUrl TEXT, "
		   "homepageUrl TEXT)");
	query.finish();
        query.exec("create table episode "
		   "(episodeId INTEGER PRIMARY KEY, "
		   "podcastId INTEGER, "
		   "title TEXT, "
		   "description TEXT, "
		   "url TEXT, "
		   "length INTEGER, "
		   "duration TEXT, "
		   "date TEXT, "
		   "played BOOL, "
		   "filePath TEXT, "
		   "playedDuration INTEGER, "
		   "FOREIGN KEY (podcastId) REFERENCES podcast(podcastId))");
	query.finish();
	_db.close();
    }
    else {
	_db.setDatabaseName(_db_path + "/" + _db_name);
    }
}

qint64 DatabaseManager::selectPodcastCount() {
    qint64 count = -1;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return -1;
    }

    QSqlQuery query;
    query.exec("SELECT COUNT(*) FROM podcast");
    if (query.next()) {
	count = query.value(0).toInt();
    }
    query.finish();

    _db.close();
    return count;
}

qint64 DatabaseManager::selectEpisodeCount() {
    qint64 count = -1;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return -1;
    }

    QSqlQuery query;
    query.exec("SELECT COUNT(*) FROM episode");
    if (query.next()) {
	count = query.value(0).toInt();
    }
    query.finish();

    _db.close();
    return count;
}

qint64 DatabaseManager::selectLatestPodcastId() {
    if (selectPodcastCount() == 0) {
	return 0;
    }

    qint64 id = -1;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return -1;
    }

    QSqlQuery query;
    query.exec("SELECT podcastId FROM podcast ORDER BY podcastId DESC LIMIT 1");
    if (query.next()) {
        id = query.value(0).toInt();
    }
    query.finish();

    _db.close();
    return id;
}

qint64 DatabaseManager::selectLatestEpisodeId() {
    if (selectEpisodeCount() == 0) {
	return 0;
    }

    qint64 id = -1;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return -1;
    }

    QSqlQuery query;
    query.exec("SELECT episodeId FROM episode ORDER BY episodeId DESC LIMIT 1");
    if (query.next()) {
        id = query.value(0).toInt();
    }
    query.finish();

    _db.close();
    return id;
}

qint64 DatabaseManager::selectPodcastIdByRss(QString rssUrl) {
    qint64 id = -1;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return -1;
    }

    QSqlQuery query;
    query.prepare("SELECT podcastId FROM podcast WHERE rssUrl=:rssUrl");
    query.bindValue(":rssUrl", rssUrl);
    if (query.exec() && query.first()) {
	id = query.value(0).toInt();
    }
    else {
	qDebug() << "Database error: " << _db.lastError();
	return -1;
    }
    query.finish();

    _db.close();
    return id;
}

QList<QObject*> DatabaseManager::selectAllPodcasts() {
    QList<QObject*> podcastList;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return podcastList;
    }

    // Get all podcasts from database
    QSqlQuery query;
    query.exec("SELECT * FROM podcast");
    while (query.next()) {
	qint64 podcastId = query.value(query.record().indexOf("podcastId")).toInt();
	QString title = query.value(query.record().indexOf("title")).toString();
	QString description = query.value(query.record().indexOf("description")).toString();
	QString rssUrl = query.value(query.record().indexOf("rssUrl")).toString();
	QString imageUrl = query.value(query.record().indexOf("imageUrl")).toString();
	QString homepageUrl = query.value(query.record().indexOf("homepageUrl")).toString();

	podcastList.append(new PodcastModel(podcastId, title, description, rssUrl, imageUrl, homepageUrl));
    }
    query.finish();

    _db.close();
    return podcastList;
}

QList<QObject*> DatabaseManager::selectAllEpisodes() {
    QList<QObject*> episodeList;

    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return episodeList;
    }

    // Get all episodes from database
    QSqlQuery query;
    query.exec("SELECT * FROM episode ORDER BY date DESC");
    while (query.next()) {
	qint64 episodeId = query.value(query.record().indexOf("episodeId")).toInt();
	qint64 podcastId = query.value(query.record().indexOf("podcastId")).toInt();
	QString title = query.value(query.record().indexOf("title")).toString();
	QString description = query.value(query.record().indexOf("description")).toString();
	QString url = query.value(query.record().indexOf("url")).toString();
	qint64 length = query.value(query.record().indexOf("length")).toInt();
	QString duration = query.value(query.record().indexOf("duration")).toString();
	QDate date = query.value(query.record().indexOf("date")).toDate();
	bool played = query.value(query.record().indexOf("played")).toBool();
	QString filePath = query.value(query.record().indexOf("filePath")).toString();
	qint64 playedDuration = query.value(query.record().indexOf("playedDuration")).toInt();

        episodeList.append(new EpisodeModel(episodeId, podcastId, title, description, url, length, duration, date, played, filePath, playedDuration));
    }
    query.finish();

    _db.close();
    return episodeList;
}

void DatabaseManager::insertNewPodcast(PodcastModel *newPodcast) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    QSqlQuery query;
    query.prepare("INSERT INTO podcast (title, description, rssUrl, imageUrl, homepageUrl)"
		  "VALUES (:title, :description, :rssUrl, :imageUrl, :homepageUrl)");
    query.bindValue(":title", newPodcast->getTitle());
    query.bindValue(":description", newPodcast->getDescription());
    query.bindValue(":rssUrl", newPodcast->getRssUrl());
    query.bindValue(":imageUrl", newPodcast->getImageUrl());
    query.bindValue(":homepageUrl", newPodcast->getHomepageUrl());

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}

void DatabaseManager::insertNewEpisode(EpisodeModel *newEpisode) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    QSqlQuery query;
    query.prepare("INSERT INTO episode (podcastId, title, description, url, length, duration, date, played, filePath, playedDuration)"
		  "VALUES (:podcastId, :title, :description, :url, :length, :duration, :date, :played, :filePath, :playedDuration)");
    query.bindValue(":podcastId", newEpisode->getPodcastId());
    query.bindValue(":title", newEpisode->getTitle());
    query.bindValue(":description", newEpisode->getDescription());
    query.bindValue(":url", newEpisode->getUrl());
    query.bindValue(":length", newEpisode->getLength());
    query.bindValue(":duration", newEpisode->getDuration());
    query.bindValue(":date", newEpisode->getDate());
    query.bindValue(":played", newEpisode->getPlayed());
    query.bindValue(":filePath", newEpisode->getFilePath());
    query.bindValue(":playedDuration", newEpisode->getPlayedDuration());

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}

void DatabaseManager::insertNewEpisodeList(QList<QObject*> episodeList) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    for(QObject *obj : episodeList) {
	EpisodeModel *newEpisode = qobject_cast<EpisodeModel*>(obj);
	QSqlQuery query;
	query.prepare("INSERT INTO episode (podcastId, title, description, url, length, duration, date, played, filePath, playedDuration)"
		      "VALUES (:podcastId, :title, :description, :url, :length, :duration, :date, :played, :filePath, :playedDuration)");
	query.bindValue(":podcastId", newEpisode->getPodcastId());
	query.bindValue(":title", newEpisode->getTitle());
	query.bindValue(":description", newEpisode->getDescription());
	query.bindValue(":url", newEpisode->getUrl());
	query.bindValue(":length", newEpisode->getLength());
	query.bindValue(":duration", newEpisode->getDuration());
	query.bindValue(":date", newEpisode->getDate());
	query.bindValue(":played", newEpisode->getPlayed());
	query.bindValue(":filePath", newEpisode->getFilePath());
	query.bindValue(":playedDuration", newEpisode->getPlayedDuration());

	if (!query.exec()) {
	    qDebug() << "Database error: " << query.lastError();
	}

	query.finish();
    }

    _db.close();
}

void DatabaseManager::updateFilePath(qint64 id, QString filePath) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    QSqlQuery query;
    query.prepare("UPDATE episode SET filePath = :filePath WHERE episodeId = :episodeId");
    query.bindValue(":filePath", filePath);
    query.bindValue(":episodeId", id);

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}

void DatabaseManager::updatePlayed(qint64 id, bool state) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    QSqlQuery query;
    query.prepare("UPDATE episode SET played = :played WHERE episodeId = :episodeId");
    query.bindValue(":played", state);
    query.bindValue(":episodeId", id);

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}

void DatabaseManager::deletePodcast(qint64 podcastId) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    QSqlQuery query;
    query.prepare("DELETE FROM podcast WHERE podcastId=:podcastId");
    query.bindValue(":podcastId", podcastId);

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }
    query.finish();

    query.prepare("DELETE FROM episode WHERE podcastId=:podcastId");
    query.bindValue(":podcastId", podcastId);

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }
    query.finish();

    _db.close();
}

void DatabaseManager::updatePlayedDuration(qint64 id, qint64 playedDuration) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    QSqlQuery query;
    query.prepare("UPDATE episode SET playedDuration = :playedDuration WHERE episodeId = :episodeId");
    query.bindValue(":playedDuration", playedDuration);
    query.bindValue(":episodeId", id);

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}

// SLOTS
void DatabaseManager::updateFilePath(QString filePath) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    EpisodeModel *episode = qobject_cast<EpisodeModel*>(sender());

    QSqlQuery query;
    query.prepare("UPDATE episode SET filePath = :filePath WHERE episodeId = :episodeId");
    query.bindValue(":filePath", filePath);
    query.bindValue(":episodeId", episode->getEpisodeId());

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}

void DatabaseManager::updatePodcastImageUrl(QString imagePath) {
    if (!_db.open()) {
	qDebug() << "Database error: " << _db.lastError();
	return;
    }

    PodcastModel *podcast = qobject_cast<PodcastModel*>(sender());

    QSqlQuery query;
    query.prepare("UPDATE podcast SET imageUrl=:imageUrl WHERE podcastId=:podcastId");
    query.bindValue(":imageUrl", imagePath);
    query.bindValue(":podcastId", podcast->getId());

    if (!query.exec()) {
	qDebug() << "Database error: " << query.lastError();
    }

    query.finish();
    _db.close();
}
