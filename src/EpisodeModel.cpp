/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "EpisodeModel.h"

EpisodeModel::EpisodeModel(qint64 episodeId,
			   qint64 podcastId,
			   QString title,
			   QString description,
			   QString url,
			   qint64 length,
			   QString duration,
			   QDate date,
			   bool played,
			   QString filePath,
			   qint64 playedDuration,
			   QObject *parent) : QObject(parent)
{
    _episodeId = episodeId;
    _podcastId = podcastId;
    _title = title;
    _description = description;
    _url = url;
    _length = length;
    _duration = duration;
    _date = date;
    _played = played;
    _filePath = filePath;
    _downloadProgress = 0;
    _playedDuration = playedDuration;
}

qint64 EpisodeModel::getEpisodeId() { return _episodeId; }
qint64 EpisodeModel::getPodcastId() { return _podcastId; }
QString EpisodeModel::getTitle() { return _title; }
QString EpisodeModel::getDescription() { return _description; }
QString EpisodeModel::getUrl() { return _url; }
qint64 EpisodeModel::getLength() { return _length; }
QString EpisodeModel::getDuration() { return _duration; }
QDate EpisodeModel::getDate() { return _date; }
Q_INVOKABLE QString EpisodeModel::getDateFormat(QString format) { return _date.toString(format); }
bool EpisodeModel::getPlayed() { return _played; }
QString EpisodeModel::getFilePath() { return _filePath; }
qint64 EpisodeModel::getDownloadProgress() { return _downloadProgress; }
qint64 EpisodeModel::getPlayedDuration() { return _playedDuration; }
QObject* EpisodeModel::getParentPodcast() { return this->parent(); }

void EpisodeModel::setEpisodeId(qint64 episodeId) {
    _episodeId = episodeId;
    emit episodeIdChanged(_episodeId);
}

void EpisodeModel::setPodcastId(qint64 podcastId) {
    _podcastId = podcastId;
    emit podcastIdChanged(_podcastId);
}

void EpisodeModel::setTitle(QString title) {
    _title = title;
    emit titleChanged(_title);
}

void EpisodeModel::setDescription(QString description) {
    _description = description;
    emit descriptionChanged(_description);
}

void EpisodeModel::setUrl(QString url) {
    _url = url;
    emit urlChanged(_url);
}

void EpisodeModel::setLength(qint64 length) {
    _length = length;
    emit lengthChanged(_length);
}

void EpisodeModel::setDuration(QString duration) {
    _duration = duration;
    emit durationChanged(_duration);
}

void EpisodeModel::setDate(QDate date) {
    _date = date;
    emit dateChanged(_date);
}

void EpisodeModel::setPlayed(bool state) {
    _played = state;
    emit playedChanged(_played);
}

void EpisodeModel::setFilePath(QString filePath) {
    _filePath = filePath;
    emit filePathChanged(_filePath);
}

void EpisodeModel::setPlayedDuration(qint64 playedDuration) {
    _playedDuration = playedDuration;
    emit playedDurationChanged(_playedDuration);
}

Q_INVOKABLE void EpisodeModel::emitAbortDownload() {
    emit abortDownload();
    _downloadProgress = 0;
    emit downloadProgressChanged(_downloadProgress);
}

// SLOTS
void EpisodeModel::downloadReady() {
    if (!(_downloadProgress >= _length)) {
	return;
    }

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    QString filePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/downloads";
    QString fileName = reply->url().fileName();

    QDir dir(filePath);
    if (!dir.exists()) {
	dir.mkpath(filePath);
    }

    QFile newFile(filePath + "/" + fileName);
    if (newFile.open(QIODevice::WriteOnly)) {
	newFile.write(reply->readAll());
	newFile.close();
	setFilePath(filePath + "/" + fileName);
    }
    else {
	qDebug() << "Failed to open new file: " << newFile.errorString();
    }

    reply->deleteLater();
}

void EpisodeModel::downloadProgressSlot(qint64 bytesReceived, qint64 bytesTotal) {
    _length = bytesTotal;
    _downloadProgress = bytesReceived;
    emit lengthChanged(_length);
    emit downloadProgressChanged(_downloadProgress);
}
