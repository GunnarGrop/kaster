/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef MEDIA_PLAYER_H
#define MEDIA_PLAYER_H

#include <QObject>
#include <QMediaPlayer>

class MediaPlayer : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool isPlaying READ isPlaying NOTIFY isPlayingChanged)
    Q_PROPERTY(qint64 position READ getPosition WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(int volume READ getVolume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(bool isMuted READ isMuted WRITE setMuted NOTIFY isMutedChanged)
    Q_PROPERTY(qint64 duration READ getDuration NOTIFY durationChanged)
    Q_PROPERTY(QString trackArtist READ getTrackArtist WRITE setTrackArtist NOTIFY trackArtistChanged)
    Q_PROPERTY(QString trackTitle READ getTrackTitle WRITE setTrackTitle NOTIFY trackTitleChanged)
    Q_PROPERTY(QString trackImage READ getTrackImage WRITE setTrackImage NOTIFY trackImageChanged)
    Q_PROPERTY(bool isReady READ isReady WRITE setIsReady NOTIFY isReadyChanged)

private:
    QMediaPlayer* _player;
    QString _currentMedia;
    qint64 _duration;
    QString _trackArtist;
    QString _trackTitle;
    QString _trackImage;
    bool _ready;

public:
    explicit MediaPlayer(QObject *parent = nullptr);

    Q_INVOKABLE void play();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void setMuted(bool muted);
    Q_INVOKABLE void setPlaybackRate(qreal rate);
    Q_INVOKABLE void setMedia(QString media_path);

    bool isPlaying();
    qint64 getPosition();
    int getVolume();
    bool isMuted();
    qint64 getDuration();
    QString getTrackArtist();
    QString getTrackTitle();
    QString getTrackImage();
    bool isReady();

    void setPosition(qint64 position);
    void setVolume(int volume);
    void setTrackArtist(QString artist);
    void setTrackTitle(QString trackTitle);
    void setTrackImage(QString trackImage);
    void setIsReady(bool state);

public slots:
    void setDuration(qint64 duration);
    void stateChanged(QMediaPlayer::State state);

signals:
    bool isPlayingChanged(bool);
    qint64 positionChanged(qint64);
    int volumeChanged(int);
    bool isMutedChanged(bool);
    qint64 durationChanged(qint64);
    QString trackArtistChanged(QString);
    QString trackTitleChanged(QString);
    QString trackImageChanged(QString);
    bool isReadyChanged(bool);

};

#endif // MEDIA_PLAYER_H
