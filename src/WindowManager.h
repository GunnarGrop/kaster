/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include <QObject>
#include <QDebug>

class WindowManager : public QObject {
    Q_OBJECT
    Q_PROPERTY(qint64 width READ getWidth WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qint64 height READ getHeight WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(bool isWideScreen READ getIsWideScreen NOTIFY isWideScreenChanged)

private:
    qint64 _width, _height;
    bool _isWideScreen;

    void checkIfWideScreen();

public:
    explicit WindowManager(QObject* parent = nullptr);

    qint64 getWidth();
    qint64 getHeight();
    bool getIsWideScreen();

    void setWidth(qint64 width);
    void setHeight(qint64 height);

signals:
    qint64 widthChanged(qint64);
    qint64 heightChanged(qint64);
    bool isWideScreenChanged(bool);

};

#endif //WINDOW_MANAGER_H
