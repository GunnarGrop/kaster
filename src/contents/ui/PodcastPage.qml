/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0

Kirigami.ScrollablePage {
    required property var podcastData
    title: podcastData.title

    UnsubscribeSheet {
	id: unsubSheet
	podcastTitle: podcastData.title
	podcastId: podcastData.id
    }

    actions.contextualActions: [
	Kirigami.Action {
            icon.name: "help-contextual"
            text: "Info"
            onTriggered: {
		pageStack.push(Qt.resolvedUrl("PodcastInfoPage.qml"), { podcastData: podcastData })
            }
        },
        Kirigami.Action {
            icon.name: "kt-remove-feeds"
            text: i18n("Unsubscibe")
            onTriggered: unsubSheet.open()
        }
    ]

    EpisodeList {
	header: Kirigami.ItemViewHeader {
	    backgroundImage.source: "file:///" + podcastData.imageUrl
        }

	list: Kaster.getEpisodeListById(podcastData.id)
    }
}
