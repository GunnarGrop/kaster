/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import QtGraphicalEffects 1.12
import org.kaster.backend 1.0

Kirigami.ScrollablePage {
    required property var podcastData
    title: podcastData.title
    Layout.fillWidth: true

    UnsubscribeSheet {
	id: unsubSheet
	podcastTitle: podcastData.title
	podcastId: podcastData.id
    }

    actions.contextualActions: [
	Kirigami.Action {
            icon.name: "globe"
            text: i18n("Homepage")
            onTriggered: {
		Qt.openUrlExternally(podcastData.homepageUrl)
            }
	},
        Kirigami.Action {
            icon.name: "kt-remove-feeds"
            text: i18n("Unsubscibe")
            onTriggered: {
		unsubSheet.open()
            }
        }
    ]

    header: Item {
	height: Kirigami.Units.gridUnit * 14
	Image {
	    id: headerImage
	    anchors.fill: parent
	    source: "file:///" + podcastData.imageUrl
	    fillMode: Image.PreserveAspectCrop
	}
	FastBlur {
            anchors.fill: headerImage
            source: headerImage
            radius: 64
	}

	GridLayout {
	    anchors {
		verticalCenter: parent.verticalCenter
		left: parent.left
		margins: Kirigami.Units.gridUnit
	    }
	    rowSpacing: Kirigami.Units.smallSpacing
            columnSpacing: Kirigami.Units.smallSpacing
            columns: 2
	    Image {
		source: "file:///" + podcastData.imageUrl
		Layout.maximumHeight: Kirigami.Units.iconSizes.enormous
		Layout.preferredWidth: height
	    }
	    ColumnLayout {
		Kirigami.Heading {
		    level: 2
		    text: podcastData.title
		    color: "#fcfaf8"
		    layer.enabled: true
		    layer.effect: TextDropShadow {}
		}
		Controls.Label {
		    text: qsTr("<a href=\"" + podcastData.homepageUrl + "\">" + podcastData.homepageUrl + "<a/>")
		    onLinkActivated: Qt.openUrlExternally(podcastData.homepageUrl)
		}
	    }
	}
    }

    ColumnLayout {
	Layout.fillWidth: parent

	Kirigami.Heading {
	    text: i18n("Description")
	}
	Controls.Label {
	    Layout.fillWidth: true
	    wrapMode: Text.WordWrap
	    text: podcastData.description
	}

	Kirigami.Heading {
	    text: i18n("RSS")
	}
	Controls.Label {
	    Layout.fillWidth: true
	    wrapMode: Text.WordWrap
	    text: qsTr("<a href=\"" + podcastData.rssUrl + "\">" + podcastData.rssUrl + "<a/>")
	    onLinkActivated: Qt.openUrlExternally(podcastData.rssUrl)
	}
    }
}
