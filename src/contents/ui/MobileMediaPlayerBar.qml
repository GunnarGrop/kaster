/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import QtGraphicalEffects 1.12
import org.kaster.backend 1.0

Item {
    id: mediaPlayerBar
    visible: MediaPlayer.isReady

    states: [
	State {
	    name: "full"
	    when: WindowManager.isWideScreen
	    PropertyChanges { target: mediaPlayerBar; visible: false }
	},
	State {
            name: "compact"
            when: !WindowManager.isWideScreen && MediaPlayer.isReady
            PropertyChanges { target: mediaPlayerBar; visible: true }
        }
    ]

    height: Kirigami.Units.gridUnit * 3
    anchors {
	left: parent.left
	right: parent.right
	bottom: parent.bottom
    }

    PlayingSheet {
	id: playingSheet
    }

    Rectangle {
	anchors.fill: parent
	color: Kirigami.Theme.backgroundColor
	MouseArea {
            anchors.fill: parent
            onClicked: { playingSheet.open() }
	}
    }

    ColumnLayout {
	anchors.fill: parent
	anchors.margins: 0
	spacing: 0

	Kirigami.Separator {
	    Layout.fillWidth: true
	}

	RowLayout {
	    Layout.fillWidth: true
	    spacing: Kirigami.Units.smallSpacing
	    Image {
		Layout.fillHeight: true
		Layout.preferredWidth: height
		fillMode: Image.PreserveAspectCrop
		source: "file:///" + MediaPlayer.trackImage
	    }
            ColumnLayout {
		id: episodeInfo
		Layout.minimumWidth: Kirigami.Units.gridUnit * 6
		Layout.maximumWidth: parent.width
		Controls.Label {
		    Layout.preferredWidth: parent.width
                    wrapMode: Text.NoWrap
                    color: Kirigami.Theme.textColor
                    text: MediaPlayer.trackArtist
		    elide: Text.ElideRight
		}
		Controls.Label {
		    Layout.preferredWidth: parent.width
                    wrapMode: Text.NoWrap
                    font.bold: true
                    color: Kirigami.Theme.textColor
                    text: MediaPlayer.trackTitle
		    elide: Text.ElideRight
		}
            }
	    Controls.ToolButton {
		id: backward
		icon.name: "media-seek-backward"
		onClicked: MediaPlayer.position = MediaPlayer.position - 10 * 1000
            }
	    Controls.ToolButton {
		id: playPause
		icon.name: MediaPlayer.isPlaying ? "media-playback-pause" : "media-playback-start"
		onClicked: MediaPlayer.isPlaying ? MediaPlayer.pause() : MediaPlayer.play()
            }
	    Controls.ToolButton  {
		id: forward
		icon.name: "media-seek-forward"
		onClicked: MediaPlayer.position = MediaPlayer.position + 10 * 1000
            }
	}

	Controls.ProgressBar {
	    id: progressBar
	    Layout.alignment: Qt.AlignBottom
	    Layout.fillWidth: true
	    Layout.fillHeight: true
	    Layout.preferredHeight: Kirigami.Units.gridUnit / 4
	    from: 0
	    to: MediaPlayer.duration
	    value: MediaPlayer.position
	    background: Rectangle {
		radius: 0
		color: Kirigami.Theme.disabledTextColor
	    }

	    contentItem: Item {
		Rectangle {
		    width: progressBar.visualPosition * parent.width
		    height: parent.height
		    radius: 2
		    color: Kirigami.Theme.highlightColor
		}
	    }
        }
    }
}
