/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0
import "functions.js" as Functions

Kirigami.ApplicationWindow {
    id: root
    pageStack.globalToolBar.style : Kirigami.ApplicationHeaderStyle.ToolBar

    minimumWidth: 480
    minimumHeight: 480

    globalDrawer: GlobalDrawer {}

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    Timer {
	interval: 5000;
	running: true;
	repeat: true;
	onTriggered: Functions.updatePlayedDuration(MediaPlayer.trackTitle, MediaPlayer.position, MediaPlayer.isPlaying)
    }

    onWidthChanged: WindowManager.width = root.width
    onHeightChanged: WindowManager.height = root.height

    pageStack.initialPage: Qt.resolvedUrl("EpisodePage.qml")

    MediaPlayerBar {}
    MobileMediaPlayerBar {}
}
