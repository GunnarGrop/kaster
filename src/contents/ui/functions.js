function bytesToString(bytes) {
    if(bytes > (1000 * 1000 * 1000))
	return Math.floor(bytes / (1000 * 1000 * 1000)) + "GB"
    else if(bytes > (1000 * 1000))
	return Math.floor(bytes/(1000 * 1000)) + "MB"
    else if(bytes > 1000)
	return Math.floor(bytes / 1000) + "kB"
    else
	return bytes + "B"
}

function msToTime(ms) {
    let seconds = Math.floor((ms / 1000) % 60);
    let minutes = Math.floor((ms / (1000 * 60)) % 60);
    let hours = Math.floor((ms / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    if(hours < 1)
        return minutes + ":" + seconds;
    else
        return hours + ":" + minutes + ":" + seconds;
}

function episodeMainButtonText(episode) {
    if (episode.filePath) {
	if (MediaPlayer.trackTitle == episode.title && MediaPlayer.isPlaying) {
	    return "Pause"
	}
	else {
	    return "Play"
	}
    }
    else {
	if (episode.downloadProgress > 0 && episode.downloadProgress < episode.length) {
	    return "Cancel"
	}
	else {
	    return "Download"
	}
    }
}

function episodeMainButtonIcon(episode) {
    if (episode.filePath) {
	if (MediaPlayer.trackTitle == episode.title && MediaPlayer.isPlaying) {
	    return "media-playback-pause"
	}
	else {
	    return "media-playback-start"
	}
    }
    else {
	if (episode.downloadProgress > 0 && episode.downloadProgress < episode.length) {
	    return "dialog-cancel"
	}
	else {
	    return "download"
	}
    }
}

function playEpisode(episode) {
    Kaster.markEpisodeAsPlayed(episode)
    MediaPlayer.setMedia(episode.filePath)
    MediaPlayer.trackArtist = episode.parentPodcast.title
    MediaPlayer.trackTitle = episode.title
    MediaPlayer.trackImage = episode.parentPodcast.imageUrl
    MediaPlayer.play()
}

function episodeMainButtonFunction(episode) {
    if (episode.filePath) {
	if (MediaPlayer.trackTitle == episode.title && MediaPlayer.isPlaying) {
	    MediaPlayer.pause()
	}
	else if (MediaPlayer.trackTitle == episode.title && !MediaPlayer.isPlaying) {
	    MediaPlayer.play()
	}
	else {
	    playEpisode(episode)
	}
    }
    else {
	if (episode.downloadProgress > 0 && episode.downloadProgress < episode.length) {
	    episode.emitAbortDownload()
	}
	else {
	    Kaster.downloadEpisode(episode)
	}
    }
}

function deleteEpisode(episode) {
    if (MediaPlayer.trackTitle == episode.title) {
	if (MediaPlayer.isPlaying) {
	    MediaPlayer.pause()
	}
	MediaPlayer.isReady = false
    }
    Kaster.deleteDownloadedEpisode(episode)
}

function updatePlayedDuration(episodeTitle, playedDuration, currentlyPlaying) {
    if (currentlyPlaying) {
	Kaster.updatePlayedDuration(episodeTitle, playedDuration)
    }
}

function durationStringToMs(durationString) {
    const words = durationString.split(":");

    if (words.length == 3) {
	let hours = words[0];
	let minutes = words[1];
	let seconds = words[2];
	return ((hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000));
    }
    else if (words.length == 2) {
	let minutes = words[0];
	let seconds = words[1];
	console.log((minutes * 60 * 1000) + (seconds * 1000));
	return ((minutes * 60 * 1000) + (seconds * 1000));
    }
    else {
	let seconds = words[0];
	return (seconds * 1000);
    }
}
