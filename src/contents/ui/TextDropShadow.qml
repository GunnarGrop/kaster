/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.14
import QtGraphicalEffects 1.12
import org.kde.kirigami 2.11 as Kirigami

DropShadow {
    property var shadowColor: "#232629"

    verticalOffset: 1
    horizontalOffset: 1
    color: shadowColor
    radius: 1
    samples: 3
}
