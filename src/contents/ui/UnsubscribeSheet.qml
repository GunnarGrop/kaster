/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import QtGraphicalEffects 1.12
import org.kaster.backend 1.0

Kirigami.OverlaySheet {
    id: globalSheet
    required property var podcastTitle
    required property var podcastId

    parent: applicationWindow().overlay

    ColumnLayout {
	Layout.alignment: Qt.AlignCenter
	Controls.Label {
	    Layout.alignment: Qt.AlignHCenter
	    Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: i18n("Are you sure you want to unsubscribe from " + podcastData.title + "?")
        }
	RowLayout {
	    Layout.alignment: Qt.AlignHCenter
	    Controls.Button {
		id: cancel
		icon.name: "dialog-cancel"
		text: i18n("cancel")
		onClicked: globalSheet.close()
	    }
	    Controls.Button {
		id: confirm
		icon.name: "dialog-ok"
		text: i18n("confirm")
		onClicked: {
		    globalSheet.close()
		    Kaster.unsubscribe(podcastData.id)
		    pageStack.clear()
		    pageStack.push(Qt.resolvedUrl("EpisodePage.qml"))
		}
	    }
	}
    }
}
