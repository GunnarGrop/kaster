/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import QtGraphicalEffects 1.12
import org.kaster.backend 1.0

Kirigami.OverlaySheet {
    id: globalSheet

    parent: applicationWindow().overlay

    function msToTime(ms) {
        let seconds = Math.floor((ms / 1000) % 60);
        let minutes = Math.floor((ms / (1000 * 60)) % 60);
        let hours = Math.floor((ms / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        if(hours < 1)
            return minutes + ":" + seconds;
        else
            return hours + ":" + minutes + ":" + seconds;
    }

    ColumnLayout {
	Layout.alignment: Qt.AlignCenter
	Image {
	    id: trackCover
	    Layout.alignment: Qt.AlignHCenter
	    source: "file:///" + MediaPlayer.trackImage
	    fillMode: Image.PreserveAspectCrop
	    Layout.maximumHeight: Kirigami.Units.iconSizes.enormous
	    Layout.preferredWidth: height
	}
	Controls.Label {
	    Layout.alignment: Qt.AlignHCenter
            wrapMode: Text.WordWrap
            text: MediaPlayer.trackArtist
        }
        Controls.Label {
	    Layout.alignment: Qt.AlignHCenter
            wrapMode: Text.WordWrap
            text: MediaPlayer.trackTitle
        }
	RowLayout {
	    Controls.Label {
		text: msToTime(MediaPlayer.position)
            }
	    Controls.Slider {
		id: progressSlider
		Layout.fillWidth: true
		Layout.minimumWidth: Kirigami.Units.longDuration * 2
		from: 0
		to: MediaPlayer.duration
		value: MediaPlayer.position
		onMoved: MediaPlayer.position = valueAt(position)
            }
	    Controls.Label {
		text: msToTime(MediaPlayer.duration)
            }
	}
	RowLayout {
	    Layout.alignment: Qt.AlignHCenter
	    Controls.ToolButton {
		id: backward
		icon.name: "media-seek-backward"
		onClicked: MediaPlayer.position = MediaPlayer.position - 10 * 1000
	    }
	    Controls.ToolButton {
		id: playPause
		icon.name: MediaPlayer.isPlaying ? "media-playback-pause" : "media-playback-start"
		onClicked: MediaPlayer.isPlaying ? MediaPlayer.pause() : MediaPlayer.play()
	    }
	    Controls.ToolButton  {
		id: forward
		icon.name: "media-seek-forward"
		onClicked: MediaPlayer.position = MediaPlayer.position + 10 * 1000
	    }
	}
	RowLayout {
	    Layout.alignment: Qt.AlignHCenter
	    Controls.ToolButton {
		text: "1.00x"
		onClicked: MediaPlayer.setPlaybackRate(1.0)
	    }
	    Controls.ToolButton {
		text: "1.25x"
		onClicked: MediaPlayer.setPlaybackRate(1.25)
	    }
	    Controls.ToolButton {
		text: "1.50x"
		onClicked: MediaPlayer.setPlaybackRate(1.5)
	    }
	    Controls.ToolButton {
		text: "1.75x"
		onClicked: MediaPlayer.setPlaybackRate(1.75)
	    }
	    Controls.ToolButton {
		text: "2.00x"
		onClicked: MediaPlayer.setPlaybackRate(2.0)
	    }
	}
    }
}
