/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import QtGraphicalEffects 1.12
import org.kaster.backend 1.0
import "functions.js" as Functions

Kirigami.ScrollablePage {
    required property var episodeData
    title: episodeData.title
    Layout.fillWidth: true

    header: Item {
	height: Kirigami.Units.gridUnit * 14
	Image {
	    id: headerImage
	    anchors.fill: parent
	    source: "file:///" + episodeData.parentPodcast.imageUrl
	    fillMode: Image.PreserveAspectCrop
	}
	FastBlur {
            anchors.fill: headerImage
            source: headerImage
            radius: 64
	}

	GridLayout {
	    anchors {
		verticalCenter: parent.verticalCenter
		left: parent.left
		margins: Kirigami.Units.gridUnit
	    }
	    rowSpacing: Kirigami.Units.smallSpacing
            columnSpacing: Kirigami.Units.smallSpacing
            columns: 2
	    Image {
		source: "file:///" + episodeData.parentPodcast.imageUrl
		Layout.maximumHeight: Kirigami.Units.iconSizes.enormous
		Layout.preferredWidth: height
		MouseArea {
		    anchors.fill: parent
		    onClicked: pageStack.push(Qt.resolvedUrl("PodcastPage.qml"), { podcastData: episodeData.parentPodcast })
		}
	    }
	    ColumnLayout {
		Kirigami.Heading {
		    level: 2
		    text: episodeData.parentPodcast.title
		    color: "#fcfaf8"
		    layer.enabled: true
		    layer.effect: TextDropShadow {}
		    MouseArea {
			anchors.fill: parent
			onClicked: pageStack.push(Qt.resolvedUrl("PodcastPage.qml"), { podcastData: episodeData.parentPodcast })
		    }
		}
		Controls.Label {
		    text: episodeData.title
		    color: "#fcfaf8"
		    layer.enabled: true
		    layer.effect: TextDropShadow {}
		}
		Controls.Label {
                    text: episodeData.getDateFormat("yyyy MMM dd") + " \u00B7 " +
			episodeData.duration + " \u00B7 " + Functions.bytesToString(episodeData.length)
		    color: "#fcfaf8"
		    layer.enabled: true
		    layer.effect: TextDropShadow {}
                }
	    }
	}
    }

    ColumnLayout {
	Layout.fillWidth: parent

	RowLayout {
	    Controls.ToolButton {
		Layout.fillWidth: parent
		text: i18n(Functions.episodeMainButtonText(episodeData));
		icon.name: Functions.episodeMainButtonIcon(episodeData)
		onClicked: Functions.episodeMainButtonFunction(episodeData)
	    }
	    Controls.ToolButton {
		Layout.fillWidth: parent
		visible: episodeData.filePath == "" ? false : true
		text: i18n("Delete")
		icon.name: "albumfolder-user-trash"
		onClicked: Functions.deleteEpisode(episodeData)
	    }
	}

	Controls.ProgressBar {
	    Layout.alignment: Qt.AlignBottom
	    Layout.columnSpan: 2
	    visible: episodeData.downloadProgress > 0 && episodeData.downloadProgress < episodeData.length ? true : false
	    Layout.fillWidth: true
	    from: 0
	    to: episodeData.length / 100
	    value: episodeData.downloadProgress / 100
	}

	Kirigami.Heading {
	    text: i18n("Description")
	}
	Controls.Label {
	    Layout.fillWidth: true
	    wrapMode: Text.WordWrap
	    text: episodeData.description
	}

	Kirigami.Heading {
	    text: i18n("URL")
	}
	Controls.Label {
	    Layout.fillWidth: true
	    wrapMode: Text.WordWrap
	    text: qsTr("<a href=\"" + episodeData.url + "\">" + episodeData.url + "<a/>")
	    onLinkActivated: Qt.openUrlExternally(episodeData.url)
	}
    }
}
