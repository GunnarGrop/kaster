/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0
import "functions.js" as Functions

ListView {
    id: episodeCardList
    required property var list
    property var dateFormat: "yyyy MMM dd"

    model: list
    spacing: Kirigami.Units.smallSpacing

    delegate: Kirigami.AbstractListItem {
	onClicked: {
	    pageStack.push(Qt.resolvedUrl("EpisodeInfoPage.qml"), { episodeData: modelData })
	}

        contentItem: Item {
            implicitWidth: delegateLayout.implicitWidth
            implicitHeight: delegateLayout.implicitHeight
            GridLayout {
                id: delegateLayout
                anchors {
                    left: parent.left
                    top: parent.top
                    right: parent.right
                }
                rowSpacing: Kirigami.Units.smallSpacing
                columnSpacing: Kirigami.Units.smallSpacing
                columns: 4
                Image {
                    source: "file:///" + modelData.parentPodcast.imageUrl
                    Layout.fillHeight: true
                    Layout.maximumHeight: Kirigami.Units.iconSizes.huge
                    Layout.preferredWidth: height
                }
                ColumnLayout {
                    Kirigami.Heading {
			Layout.fillWidth: true
                        level: 2
                        text: modelData.title
			color: modelData.played ? Kirigami.Theme.disabledTextColor : Kirigami.Theme.textColor
			elide: Text.ElideRight
                    }
                    Controls.Label {
                        Layout.fillWidth: true
                        text: modelData.getDateFormat(dateFormat) + " \u00B7 " + Functions.bytesToString(modelData.length)
			color: Kirigami.Theme.disabledTextColor
			elide: Text.ElideRight
                    }
		    RowLayout {
			Controls.Label {
			    visible: modelData.played ? true : false
			    text: Functions.msToTime(modelData.playedDuration)
			    color: Kirigami.Theme.disabledTextColor
			}
			Controls.ProgressBar {
			    visible: modelData.played ? true : false
			    Layout.preferredHeight: Kirigami.Units.gridUnit / 6
			    Layout.fillWidth: true
			    from: 0
			    to: Functions.durationStringToMs(modelData.duration);
			    value: modelData.playedDuration
			}
			Controls.Label {
			    text: modelData.duration
			    color: Kirigami.Theme.disabledTextColor
			}
		    }
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignRight|Qt.AlignVCenter
                    Layout.columnSpan: 2
                    icon.name: Functions.episodeMainButtonIcon(modelData)
		    onClicked: Functions.episodeMainButtonFunction(modelData)
                }
		Controls.ProgressBar {
		    Layout.alignment: Qt.AlignBottom
		    Layout.columnSpan: 2
		    visible: modelData.downloadProgress > 0 && modelData.downloadProgress < modelData.length ? true : false
		    Layout.fillWidth: true
		    from: 0
		    to: modelData.length / 100
		    value: modelData.downloadProgress / 100
		}
            }
        }
    }
}
