/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0

Kirigami.OverlaySheet {
    id: addNewPodcastSheet
    parent: applicationWindow().overlay

    ColumnLayout {
        spacing: Kirigami.Units.largeSpacing
        Layout.preferredWidth: Kirigami.Units.gridUnit
        Controls.Label {
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: i18n("Add new podcast")
        }
        RowLayout {
            Controls.TextField {
                id: textField
                placeholderText: i18n("RSS feed...")
                Layout.fillWidth: true
                onAccepted: [
                    addNewPodcastSheet.close(),
                    //pageStack.push(searchForSubredditPageComponent, {searchQuery: qsTr(searchField.text)}),
                    textField.text = ""
                ]
            }
            Controls.Button {
                text: i18n("Go")
                onClicked: [
                    addNewPodcastSheet.close(),
                    //pageStack.push(Qt.resolvedUrl("startPage.qml"), {searchQuery: qsTr(searchField.text)}),
		    Kaster.addNewPodcast(qsTr(textField.text)),
                    textField.text = ""
                ]
            }
        }
    }
}
