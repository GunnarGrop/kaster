/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0

Kirigami.ScrollablePage {
    id: page
    title: i18nc("@title:page:latest", "Latest")

    actions {
	main: Kirigami.Action {
	    icon.name: "view-sort"
	    text: i18n("View")
	    Kirigami.Action {
		text: i18n("Latest")
		onTriggered: {
		    episodeList.list = Kaster.latestEpisodeList
		    page.title = i18nc("@title:page:latest", "Latest")
		}
	    }
            Kirigami.Action {
		text: i18n("All")
		onTriggered: {
		    episodeList.list = Kaster.episodeList
		    episodeList.dateFormat = "yyyy MMM dd"
		    page.title = i18nc("@title:page:latest", "All")
		}
            }
	}
    }

    EpisodeList {
	id: episodeList
	list: Kaster.latestEpisodeList
	dateFormat: "MMM dd"
    }
}
