/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0

Kirigami.AboutPage {
    aboutData: Kaster.aboutData
}
