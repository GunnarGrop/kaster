/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0

Kirigami.ScrollablePage {
    title: i18nc("@title:page:podcastList", "Podcasts")

    Kirigami.CardsGridView {
        id: view
        model: Kaster.podcastList
	maximumColumnWidth: Kirigami.Units.gridUnit * 15

        delegate: Kirigami.Card {
            id: card
            banner {
		titleAlignment: Qt.AlignLeft|Qt.AlignBottom
                title: modelData.title
                source: "file:///" + modelData.imageUrl
            }
	    onClicked: {
		pageStack.push(Qt.resolvedUrl("PodcastPage.qml"), { podcastData: modelData })
	    }
        }
    }
}
