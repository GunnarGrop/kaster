/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0

Kirigami.GlobalDrawer {
    id: globalDrawer
    modal: !WindowManager.isWideScreen
    title: qsTr("Kaster")

    width: Kirigami.Units.gridUnit * 14

    header: Kirigami.AbstractApplicationHeader {
        contentItem: RowLayout {
            anchors {
                left: parent.left
                leftMargin: Kirigami.Units.smallSpacing
                right: parent.right
                rightMargin: Kirigami.Units.smallSpacing
		top: parent.top
            }
	    Kirigami.Heading {
		text: "Kaster"
	    }
        }
    }

    AddNewPodcastSheet {
	id: addNewPodcastSheet
    }

    Item {
	states: [
	    State {
		name: "full"
		when: WindowManager.isWideScreen
		PropertyChanges { target: globalDrawer; drawerOpen: true  }
	    },
	    State {
                name: "compact"
                when: !WindowManager.isWideScreen
                PropertyChanges { target: globalDrawer; drawerOpen: false }
            }
	]
    }

    actions: [
	Kirigami.Action {
	    text: i18n("Podcasts")
	    icon.name: "rss"
	    onTriggered: {
		pageStack.clear()
		pageStack.push(Qt.resolvedUrl("PodcastListPage.qml"))
	    }
	},
	Kirigami.Action {
	    text: i18n("Episodes")
	    icon.name: "rss"
	    onTriggered: {
		pageStack.clear()
		pageStack.push(Qt.resolvedUrl("EpisodePage.qml"))
	    }
	},
	Kirigami.Action {
	    text: i18n("Downloads")
	    icon.name: "download"
	    onTriggered: {
		pageStack.clear()
		pageStack.push(Qt.resolvedUrl("DownloadedEpisodesPage.qml"))
	    }
	},
	Kirigami.Action {
	    text: i18n("Add new podcast")
	    icon.name: "kt-add-feeds"
	    onTriggered: addNewPodcastSheet.open()
	}
    ]

    ColumnLayout {
        spacing: 0
        Layout.fillWidth: true

        Kirigami.Separator {
            Layout.fillWidth: true
        }

	Kirigami.BasicListItem {
	    anchors.margins: 0
	    icon: "help-about"
	    label: i18n("About")
	    onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
	}
    }
}
