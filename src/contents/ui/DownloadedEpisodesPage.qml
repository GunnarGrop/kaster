/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import org.kaster.backend 1.0
import "functions.js" as Functions

Kirigami.ScrollablePage {
    title: i18nc("@title:page:downloadedEpisodes", "Downloaded Episodes")

    actions.contextualActions: [
	Kirigami.Action {
            icon.name: "albumfolder-user-trash"
            text: i18n("Delete all episodes")
            onTriggered: Kaster.deleteAllDownloadedEpisodes()
	}
    ]

    ListView {
	id: episodeList

	model: Kaster.downloadedEpisodeList
	spacing: Kirigami.Units.smallSpacing

	delegate: Kirigami.AbstractListItem {
	    onClicked: {
		pageStack.push(Qt.resolvedUrl("EpisodeInfoPage.qml"), { episodeData: modelData })
	    }

            contentItem: Item {
		implicitWidth: delegateLayout.implicitWidth
		implicitHeight: delegateLayout.implicitHeight
		GridLayout {
                    id: delegateLayout
                    anchors {
			left: parent.left
			top: parent.top
			right: parent.right
                    }
                    rowSpacing: Kirigami.Units.smallSpacing
                    columnSpacing: Kirigami.Units.smallSpacing
                    columns: 4
                    Image {
			source: "file:///" + modelData.parentPodcast.imageUrl
			Layout.fillHeight: true
			Layout.maximumHeight: Kirigami.Units.iconSizes.huge
			Layout.preferredWidth: height
                    }
                    ColumnLayout {
			Kirigami.Heading {
			    Layout.fillWidth: true
                            level: 2
                            text: modelData.title
			    color: modelData.played ? Kirigami.Theme.disabledTextColor : Kirigami.Theme.textColor
			    elide: Text.ElideRight
			}
			Controls.Label {
                            Layout.fillWidth: true
                            text: modelData.getDateFormat("yyyy MMM dd") + " \u00B7 " + modelData.duration + " \u00B7 " + Functions.bytesToString(modelData.length)
			    elide: Text.ElideRight
			}
                    }
                    Controls.Button {
			Layout.alignment: Qt.AlignRight|Qt.AlignVCenter
			Layout.columnSpan: 2
			icon.name: "albumfolder-user-trash"
			onClicked: Functions.deleteEpisode(modelData)
                    }
		}
            }
	}
    }
}
