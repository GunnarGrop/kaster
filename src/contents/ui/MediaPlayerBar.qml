/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.11 as Kirigami
import QtGraphicalEffects 1.12
import org.kaster.backend 1.0
import "functions.js" as Functions

Item {
    id: mediaPlayerBar
    visible: MediaPlayer.isReady

    states: [
	State {
	    name: "full"
	    when: WindowManager.isWideScreen && MediaPlayer.isReady
	    PropertyChanges { target: mediaPlayerBar; visible: true }
	},
	State {
            name: "compact"
            when: !WindowManager.isWideScreen
            PropertyChanges { target: mediaPlayerBar; visible: false }
        }
    ]

    height: Kirigami.Units.gridUnit * 3
    anchors {
	left: parent.left
	right: parent.right
	bottom: parent.bottom
    }

    Rectangle {
	id: backgroundRect
	anchors.fill: parent
	color: Kirigami.Theme.backgroundColor
    }

    ColumnLayout {
	anchors.fill: parent
	spacing: 0
	Kirigami.Separator {
            Layout.fillWidth: true
	}
	RowLayout {
	    Layout.alignment: parent.center
	    Layout.leftMargin: Kirigami.Units.smallSpacing
	    Layout.rightMargin: Kirigami.Units.smallSpacing
            Controls.ToolButton {
		id: backward
		icon.name: "media-seek-backward"
		onClicked: MediaPlayer.position = MediaPlayer.position - 10 * 1000
            }
            Controls.ToolButton {
		id: playPause
		icon.name: MediaPlayer.isPlaying ? "media-playback-pause" : "media-playback-start"
		onClicked: MediaPlayer.isPlaying ? MediaPlayer.pause() : MediaPlayer.play()
            }
            Controls.ToolButton  {
		id: forward
		icon.name: "media-seek-forward"
		onClicked: MediaPlayer.position = MediaPlayer.position + 10 * 1000
            }
            ColumnLayout {
		id: episodeInfo
		Layout.minimumWidth: Kirigami.Units.gridUnit * 6
		Layout.maximumWidth: Kirigami.Units.gridUnit * 10
		Controls.Label {
		    Layout.preferredWidth: parent.width
                    wrapMode: Text.NoWrap
                    color: Kirigami.Theme.textColor
                    text: MediaPlayer.trackArtist
		    elide: Text.ElideRight
		}
		Controls.Label {
		    Layout.preferredWidth: parent.width
                    wrapMode: Text.NoWrap
                    font.bold: true
                    color: Kirigami.Theme.textColor
                    text: MediaPlayer.trackTitle
		    elide: Text.ElideRight
		}
            }
            Controls.Slider {
		id: progressSlider
		Layout.fillWidth: true
		Layout.minimumWidth: Kirigami.Units.longDuration * 2
		from: 0
		to: MediaPlayer.duration
		value: MediaPlayer.position
		onMoved: MediaPlayer.position = valueAt(position)
            }
            Controls.Label {
		text: Functions.msToTime(MediaPlayer.position)
            }
            Controls.Label {
		text: "/"
            }
            Controls.Label {
		text: Functions.msToTime(MediaPlayer.duration)
            }
            Controls.ToolButton  {
		id: mute
		icon.name: MediaPlayer.isMuted ? "audio-volume-muted" : "audio-volume-high"
		onClicked: MediaPlayer.isMuted ? MediaPlayer.setMuted(false) : MediaPlayer.setMuted(true)
            }
            Controls.Slider {
		id: volumeSlider
		enabled: !MediaPlayer.isMuted
		Layout.minimumWidth: Kirigami.Units.gridUnit * 2
		Layout.maximumWidth: Kirigami.Units.gridUnit * 5
		value: MediaPlayer.volume
		to: 100
		onMoved: MediaPlayer.volume = valueAt(position)
            }
            Controls.ComboBox {
		id: playbackRate
		Layout.maximumWidth: Kirigami.Units.gridUnit * 4
		Layout.maximumHeight: Kirigami.Units.gridUnit * 2
		textRole: "text"
		model: [{"text": "1.00x", "value": 1.0},
			{"text": "1.25x", "value": 1.25},
			{"text": "1.50x", "value": 1.50},
			{"text": "1.75x", "value": 1.75},
			{"text": "2.00x", "value": 2.0}]
		onActivated: {
                    MediaPlayer.setPlaybackRate(model[index].value)
		}
            }
	}
    }


}
