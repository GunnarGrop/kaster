/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "RssParser.h"

RssParser::RssParser(QIODevice *device) {
    _streamReader = new QXmlStreamReader(device);
}

bool RssParser::isValidDocument() {
    bool isValid = true;

    _streamReader->readNextStartElement();
    if (_streamReader->name().toString() != "rss") {
	isValid = false;
    }
    _streamReader->readNextStartElement();
    if (_streamReader->name().toString() != "channel") {
	isValid = false;
    }

    return isValid;
}

// Reads the beggning elements of the XML document
PodcastModel* RssParser::readPodcastInfo(qint64 newPodcastId, QString rssUrl) {
    QString title, description, imageUrl, homepageUrl;

    while (!_streamReader->atEnd()) {
        _streamReader->readNextStartElement();
	QString element = _streamReader->name().toString();

	if (_streamReader->isStartElement()) {
	    if (element == "title") {
		title = _streamReader->readElementText();
	    }
	    else if (element == "link") {
		homepageUrl = _streamReader->readElementText();
	    }
	    else if (element == "description") {
		description = _streamReader->readElementText();
	    }
	    else if (element == "image" && _streamReader->attributes().hasAttribute("href")) {
	        imageUrl = _streamReader->attributes().value("href").toString();
	    }
	    else if (element == "item") { // End of podcast info, beginning of episode info
		break;
	    }
	}
    }

    if (_streamReader->hasError()) {
	qDebug() << _streamReader->errorString();
    }

    //qDebug() << "Title: " << title << "\nDesc: " << description << "\nImageUrl: " << imageUrl << "\nHomepageUrl: " << homepageUrl;
    return new PodcastModel(newPodcastId, title, description, rssUrl, imageUrl, homepageUrl);
}

// Reads an <item></item> element
EpisodeModel* RssParser::readEpisodeInfo(qint64 newEpisodeId, qint64 newPodcastId) {
    QString title, description, url, duration;
    qint64 length = 0;
    QDate date;
    QLocale::setDefault(QLocale::English); // assuming all RSS feeds use english date locales

    while (!_streamReader->atEnd()) {
	_streamReader->readNextStartElement();
	QString element = _streamReader->name().toString();

	if (_streamReader->isStartElement()) {
	    if (element == "title") {
		title = _streamReader->readElementText();
	    }
	    else if (element == "description") {
		description = _streamReader->readElementText();
	    }
	    else if (element == "enclosure") {
		url = _streamReader->attributes().value("url").toString();
		length = _streamReader->attributes().value("length").toInt();
	    }
	    else if (element == "duration") {
		duration = _streamReader->readElementText();
	    }
	    else if (element == "pubDate") {
		QStringList datePieces = _streamReader->readElementText().split(" ");
		QString dateString = datePieces.at(1) + "/" + datePieces.at(2) + "/" + datePieces.at(3);
	        date = QLocale().toDate(dateString, "dd/MMM/yyyy");

	    }
	    else if (element == "item") { // Reached end of this item element
		break;
	    }
	}
    }

    QLocale::setDefault(QLocale::system()); // reset default to the systems locale
    //qDebug() << "ID: " << newPodcastId << "\nTitle: " << title << "\nDesc: " << description << "\nUrl: " << url << "\nLength: " << length << "\nDuration: " << duration << "\nDate: " << date.toString();
    return new EpisodeModel(newEpisodeId, newPodcastId, title, description, url, length, duration, date);
}

bool RssParser::episodeAvailable() {
    return (_streamReader->isStartElement() &&
	    _streamReader->name().toString() == "item");
}
