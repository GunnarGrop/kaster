/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "Kaster.h"

Kaster::Kaster(QObject *parent) : QObject(parent) {
    _databaseManager = new DatabaseManager(this);
    _downloadManager = new DownloadManager(this);

    _podcastList = _databaseManager->selectAllPodcasts();
    emit podcastListChanged(_podcastList);
    reloadEpisodeList();

    _updateTimerInterval = 3600000;
    _updateTimer = new QTimer(this);
    connect(_updateTimer, SIGNAL(timeout()),
	    this, SLOT(updateTimerTimeout()));
    _updateTimer->start(_updateTimerInterval);

    updatePodcasts();
}

void Kaster::reloadEpisodeList() {
    // setting pointers of all podcasts on index = podcast.id
    std::vector<PodcastModel*> podcastPointers;
    for (int i = 0; i < _podcastList.length(); ++i) {
        PodcastModel* podcast = qobject_cast<PodcastModel*>(_podcastList.at(i));
        podcastPointers.push_back(podcast);
    }

    // setting parent for each episode to a podcast in accordance to episode.podcastId
    _episodeList.clear();
    _episodeList = _databaseManager->selectAllEpisodes();
    for (QObject* episodeObject : _episodeList) {
	EpisodeModel* episode = qobject_cast<EpisodeModel*>(episodeObject);

	for (PodcastModel* podcast : podcastPointers) {
	    if (podcast->getId() == episode->getPodcastId()) {
		episode->setParent(podcast);
	    }
	}
    }

    emit episodeListChanged(_episodeList);
    emit latestEpisodeListChanged(getEpisodeListFromDate());
    emit downloadedEpisodeListChanged(getDownloadedEpisodesList());
}

KAboutData Kaster::getAboutData() { return KAboutData::applicationData(); }
QList<QObject*> Kaster::getPodcastList() { return _podcastList; }
QList<QObject*> Kaster::getEpisodeList() { return _episodeList; }

Q_INVOKABLE void Kaster::addNewPodcast(QString url) {
    QNetworkReply *reply = _downloadManager->get(QUrl(url));
    connect(reply, SIGNAL(finished()),
	    this, SLOT(podcastDownloadFinished()));
}

Q_INVOKABLE void Kaster::downloadEpisode(EpisodeModel *episode) {
    QNetworkReply *reply = _downloadManager->get(QUrl(episode->getUrl()));
    connect(reply, SIGNAL(finished()),
	    episode, SLOT(downloadReady()));
    connect(reply, SIGNAL(downloadProgress(qint64, qint64)),
	    episode, SLOT(downloadProgressSlot(qint64, qint64)));
    connect(episode, SIGNAL(filePathChanged(QString)),
	    _databaseManager, SLOT(updateFilePath(QString)));
    connect(episode, SIGNAL(abortDownload()), reply, SLOT(abort()));
}

Q_INVOKABLE QList<QObject*> Kaster::getEpisodeListById(qint64 podcastId) {
    QList<QObject*> episodes;

    for (QObject* episode : _episodeList) {
        EpisodeModel* tmp = qobject_cast<EpisodeModel*>(episode);
	if (tmp->getPodcastId() == podcastId) {
	    episodes.append(tmp);
	}
    }
    return episodes;
}

Q_INVOKABLE QList<QObject*> Kaster::getEpisodeListFromDate(QDate date) {
    QList<QObject*> episodes;

    for (QObject* episode : _episodeList) {
        EpisodeModel* tmp = qobject_cast<EpisodeModel*>(episode);
	if (tmp->getDate() >= date) {
	    episodes.append(tmp);
	}
    }
    return episodes;
}

Q_INVOKABLE QList<QObject*> Kaster::getDownloadedEpisodesList() {
    QList<QObject*> episodes;

    for (QObject* episode : _episodeList) {
        EpisodeModel* tmp = qobject_cast<EpisodeModel*>(episode);
	if (!tmp->getFilePath().isEmpty()) {
	    episodes.append(tmp);
	}
    }
    return episodes;
}

Q_INVOKABLE void Kaster::deleteDownloadedEpisode(EpisodeModel* episode) {
    if (QFile::remove(episode->getFilePath())) {
	episode->setFilePath("");
	_databaseManager->updateFilePath(episode->getEpisodeId(), "");
	emit downloadedEpisodeListChanged(getDownloadedEpisodesList());
    }
    else {
	qDebug() << "IO ERROR: Failed to delete file " << episode->getFilePath();
    }
}

Q_INVOKABLE void Kaster::deleteAllDownloadedEpisodes() {
    for (QObject* episode : _episodeList) {
	EpisodeModel* tmp = qobject_cast<EpisodeModel*>(episode);
	if (!tmp->getFilePath().isEmpty()) {
	    deleteDownloadedEpisode(tmp);
	}
    }
}

Q_INVOKABLE void Kaster::markEpisodeAsPlayed(EpisodeModel* episode) {
    episode->setPlayed(true);
    _databaseManager->updatePlayed(episode->getEpisodeId(), true);
}

Q_INVOKABLE void Kaster::updatePodcasts() {
    qDebug() << "Updating podcasts...";
    for (QObject* podcastObject : _podcastList) {
	PodcastModel* podcast = qobject_cast<PodcastModel*>(podcastObject);
	QNetworkReply *reply = _downloadManager->get(QUrl(podcast->getRssUrl()));
	connect(reply, SIGNAL(finished()),
		this, SLOT(podcastUpdateDownloadFinished()));
    }
}

Q_INVOKABLE void Kaster::unsubscribe(qint64 podcastId) {
    if (_podcastList.length() <= 1) {
	QString podcastImage = qobject_cast<PodcastModel*>(_podcastList.first())->getImageUrl();
	if (!QFile::remove(podcastImage)) {
	    qDebug() << "IO ERROR: Failed to delete file " << podcastImage;
	}
	_databaseManager->deletePodcast(podcastId);
	_episodeList.clear();
	_podcastList.clear();

	emit episodeListChanged(_episodeList);
	emit latestEpisodeListChanged(getEpisodeListFromDate());
	emit podcastListChanged(_podcastList);
	return;
    }

    qint64 podcastListIndex = 0;
    while (qobject_cast<PodcastModel*>(_podcastList.at(podcastListIndex))->getId() != podcastId) {
	podcastListIndex++;
    }

    QList<qint64> episodeIndices;
    for (int i = 0; i < _episodeList.length(); ++i) {
	if (qobject_cast<EpisodeModel*>(_episodeList.at(i))->getPodcastId() == podcastId) {
	    episodeIndices.append(i);
	}
    }

    QString podcastImage = qobject_cast<PodcastModel*>(_podcastList.at(podcastListIndex))->getImageUrl();
    if (!QFile::remove(podcastImage)) {
	qDebug() << "IO ERROR: Failed to delete file " << podcastImage;
    }
    _databaseManager->deletePodcast(podcastId);

    for (int i = 0; i < episodeIndices.length(); ++i) {
	if (!qobject_cast<EpisodeModel*>(_episodeList.at(i))->getFilePath().isEmpty()) {
	    deleteDownloadedEpisode(qobject_cast<EpisodeModel*>(_episodeList.at(i)));
	}
	_episodeList.removeAt(episodeIndices.at(i) - i);
    }
    _podcastList.removeAt(podcastListIndex);

    emit episodeListChanged(_episodeList);
    emit latestEpisodeListChanged(getEpisodeListFromDate());
    emit podcastListChanged(_podcastList);
}

Q_INVOKABLE void Kaster::updatePlayedDuration(QString episodeTitle, qint64 playedDuration) {
    EpisodeModel* episode = nullptr;

    /* episode title is not a unique field, thus this may find the wrong episode */
    for (QObject* episodeObj : _episodeList) {
	EpisodeModel* episodeMod = qobject_cast<EpisodeModel*>(episodeObj);
	if (episodeMod->getTitle() == episodeTitle) {
	    episode = episodeMod;
	}
    }

    if (episode) {
	episode->setPlayedDuration(playedDuration);
	_databaseManager->updatePlayedDuration(episode->getEpisodeId(), playedDuration);
    }
}

// SLOTS
void Kaster::updateTimerTimeout() {
    updatePodcasts();
}

void Kaster::podcastDownloadFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    RssParser *parser = new RssParser(reply);

    if (!parser->isValidDocument()) {
	qDebug() << "RSS parsing error: Invalid RSS document" << reply->readAll();
	reply->deleteLater();
	return;
    }

    qint64 newPodcastId = _databaseManager->selectLatestPodcastId() + 1;
    PodcastModel *podcast = parser->readPodcastInfo(newPodcastId, reply->url().toString());

    // Download podcast cover image
    QNetworkReply* imageReply = _downloadManager->get(podcast->getImageUrl());
    connect(imageReply, SIGNAL(finished()), podcast, SLOT(imageDownloadReady()));
    connect(podcast, SIGNAL(imageUrlChanged(QString)),
	    _databaseManager, SLOT(updatePodcastImageUrl(QString)));

    qint64 newEpisodeId = _databaseManager->selectLatestEpisodeId() + 1;
    QList<QObject*> episodeList;
    while (parser->episodeAvailable()) {
	episodeList.append(parser->readEpisodeInfo(newEpisodeId, newPodcastId));
	episodeList.last()->setParent(podcast);
	newEpisodeId++;
    }

    _databaseManager->insertNewPodcast(podcast);
    _podcastList.append(podcast);
    emit podcastListChanged(_podcastList);

    _databaseManager->insertNewEpisodeList(episodeList);
    reloadEpisodeList();

    reply->deleteLater();
}

void Kaster::podcastUpdateDownloadFinished() {
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    RssParser *parser = new RssParser(reply);

    if (!parser->isValidDocument()) {
	qDebug() << "RSS parsing error: Invalid RSS document" << reply->readAll();
	reply->deleteLater();
	return;
    }

    qint64 podcastId = _databaseManager->selectPodcastIdByRss(reply->url().toString());

    if (podcastId == -1) {
	qDebug() << "ERROR: Could not select id of podcast with rssUrl: " << reply->url().toString();
	reply->deleteLater();
	return;
    }

    EpisodeModel* latestEpisode;
    for (QObject* episodeObject : _episodeList) {
	EpisodeModel* episode = qobject_cast<EpisodeModel*>(episodeObject);
	if (episode->getPodcastId() == podcastId) {
	    latestEpisode = episode;
	    break;
	}
    }

    // Skip over podcast info
    parser->readPodcastInfo(-1, "");

    qint64 newEpisodeId = _episodeList.length() + 1;
    QList<QObject*> episodeList;
    while (parser->episodeAvailable()) {
	EpisodeModel* newEpisode = parser->readEpisodeInfo(newEpisodeId, podcastId);
	qDebug() << "Latest date: " << latestEpisode->getDate().toString() << "\tNew date: " <<  newEpisode->getDate().toString();
	if (newEpisode->getDate() == latestEpisode->getDate()) {
	    break;
	}
	episodeList.append(newEpisode);
	newEpisodeId++;
    }

    if (!episodeList.isEmpty()) {
	_databaseManager->insertNewEpisodeList(episodeList);
	reloadEpisodeList();
    }

    reply->deleteLater();
}
