/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "MediaPlayer.h"

MediaPlayer::MediaPlayer(QObject *parent) : QObject(parent) {
    _player = new QMediaPlayer(this);
    _player->setAudioRole(QAudio::VoiceCommunicationRole);

    connect(_player, SIGNAL(durationChanged(qint64)),
	    this, SLOT(setDuration(qint64)));
    connect(_player, SIGNAL(positionChanged(qint64)),
	    this, SIGNAL(positionChanged(qint64)));
    connect(_player, SIGNAL(stateChanged(QMediaPlayer::State)),
	    this, SLOT(stateChanged(QMediaPlayer::State)));

    _player->setVolume(10);
    _ready = false;
}

Q_INVOKABLE void MediaPlayer::play() {
    if(_player->currentMedia().isNull()) {
        qDebug() << "ERROR: No playable media currently selected";
        return;
    }

    _ready = true;
    emit isReadyChanged(_ready);
    _player->play();
}

Q_INVOKABLE void MediaPlayer::pause() {
    _player->pause();
}

Q_INVOKABLE void MediaPlayer::setMuted(bool muted) {
    _player->setMuted(muted);
    emit isMutedChanged(_player->isMuted());
}

Q_INVOKABLE void MediaPlayer::setPlaybackRate(qreal rate) {
    _player->setPlaybackRate(rate);
}

Q_INVOKABLE void MediaPlayer::setMedia(QString mediaPath) {
    _player->setMedia(QUrl::fromLocalFile(mediaPath));
}

bool MediaPlayer::isPlaying() { return (_player->state() == QMediaPlayer::State::PlayingState); }
qint64 MediaPlayer::getPosition() { return _player->position(); }
int MediaPlayer::getVolume() { return _player->volume(); }
bool MediaPlayer::isMuted() { return _player->isMuted(); }
qint64 MediaPlayer::getDuration() { return _duration; }
QString MediaPlayer::getTrackArtist() { return _trackArtist; }
QString MediaPlayer::getTrackTitle() { return _trackTitle; }
QString MediaPlayer::getTrackImage() { return _trackImage; }
bool MediaPlayer::isReady() { return _ready; }

void MediaPlayer::setPosition(qint64 position) {
    _player->setPosition(position);
    emit positionChanged(_player->position());
}

void MediaPlayer::setVolume(int volume) {
    _player->setVolume(volume);
    emit volumeChanged(_player->volume());
}

void MediaPlayer::setTrackArtist(QString artist) {
    _trackArtist = artist;
    emit trackArtistChanged(_trackArtist);
}

void MediaPlayer::setTrackTitle(QString title) {
    _trackTitle = title;
    emit trackTitleChanged(_trackTitle);
}

void MediaPlayer::setTrackImage(QString image) {
    _trackImage = image;
    emit trackImageChanged(_trackImage);
}

void MediaPlayer::setIsReady(bool state) {
    _ready = state;
    emit isReadyChanged(_ready);
}

/* SLOTS */
void MediaPlayer::setDuration(qint64 duration) {
    _duration = duration;
    emit durationChanged(_duration);
}

void MediaPlayer::stateChanged(QMediaPlayer::State state) {
    if(state == QMediaPlayer::State::PlayingState) {
        emit isPlayingChanged(true);
    }
    else {
        emit isPlayingChanged(false);
    }
}
