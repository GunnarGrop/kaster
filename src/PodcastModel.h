/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PODCAST_MODEL_H
#define PODCAST_MODEL_H

#include <QObject>
#include "EpisodeModel.h"

class PodcastModel : public QObject {
    Q_OBJECT

    Q_PROPERTY(qint64 id READ getId NOTIFY idChanged)
    Q_PROPERTY(QString title READ getTitle NOTIFY titleChanged)
    Q_PROPERTY(QString description READ getDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString rssUrl READ getRssUrl NOTIFY rssUrlChanged)
    Q_PROPERTY(QString imageUrl READ getImageUrl NOTIFY imageUrlChanged)
    Q_PROPERTY(QString homepageUrl READ getHomepageUrl NOTIFY homepageUrlChanged)
    Q_PROPERTY(QList<QObject*> episodeList READ getEpisodeList NOTIFY episodeListChanged)

private:
    qint64 _id;
    QString _title;
    QString _description;
    QString _rssUrl;
    QString _imageUrl;
    QString _homepageUrl;
    QList<QObject*> _episodeList;

public:
    explicit PodcastModel(qint64 id = 0,
			  QString title = "",
			  QString description = "",
			  QString rssUrl = "",
			  QString imageUrl = "",
			  QString homepageUrl = "",
			  QList<QObject*> episodeList = QList<QObject*>(),
			  QObject *parent = nullptr);

    qint64 getId();
    QString getTitle();
    QString getDescription();
    QString getRssUrl();
    QString getImageUrl();
    QString getHomepageUrl();
    QList<QObject*> getEpisodeList();

    void setId(qint64 id);
    void setTitle(QString title);
    void setDescription(QString description);
    void setRssUrl(QString rssUrl);
    void setImageUrl(QString imageUrl);
    void setHomepageUrl(QString homepageUrl);
    void setEpisodeList(QList<QObject*> episodeList);
    void appendEpisode(EpisodeModel* episode);

signals:
    void idChanged(qint64);
    void titleChanged(QString);
    void descriptionChanged(QString);
    void rssUrlChanged(QString);
    void imageUrlChanged(QString);
    void homepageUrlChanged(QString);
    void episodeListChanged(QList<QObject*>);

public slots:
    void imageDownloadReady();
};


#endif // PODCAST_MODEL_H
