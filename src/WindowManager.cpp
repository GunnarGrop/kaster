/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "WindowManager.h"

WindowManager::WindowManager(QObject* parent) : QObject(parent) {
    _width = 0;
    _height = 0;
    _isWideScreen = false;
}

qint64 WindowManager::getWidth() { return _width; }
qint64 WindowManager::getHeight() { return _height; }
bool WindowManager::getIsWideScreen() { return _isWideScreen; }

void WindowManager::setWidth(qint64 width) {
    _width = width;
    emit widthChanged(_width);
    checkIfWideScreen();
}

void WindowManager::setHeight(qint64 height) {
    _height = height;
    emit heightChanged(_height);
    checkIfWideScreen();
}

void WindowManager::checkIfWideScreen() {
    bool lastValue = _isWideScreen;

    if (_width > _height) {
	_isWideScreen = true;
    }
    else {
	_isWideScreen = false;
    }

    if (_isWideScreen != lastValue) {
	emit isWideScreenChanged(_isWideScreen);
    }
}
