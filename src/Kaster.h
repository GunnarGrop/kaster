/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef KASTER_H
#define KASTER_H

#include <KAboutData>
#include <QObject>
#include <QtNetwork>
#include <QTimer>
#include "PodcastModel.h"
#include "EpisodeModel.h"
#include "DatabaseManager.h"
#include "RssParser.h"
#include "DownloadManager.h"

class Kaster : public QObject {
    Q_OBJECT

    Q_PROPERTY(QList<QObject*> podcastList READ getPodcastList NOTIFY podcastListChanged);
    Q_PROPERTY(QList<QObject*> episodeList READ getEpisodeList NOTIFY episodeListChanged);
    Q_PROPERTY(QList<QObject*> latestEpisodeList READ getEpisodeListFromDate NOTIFY latestEpisodeListChanged);
    Q_PROPERTY(QList<QObject*> downloadedEpisodeList READ getDownloadedEpisodesList NOTIFY downloadedEpisodeListChanged);
    Q_PROPERTY(KAboutData aboutData READ getAboutData CONSTANT);

private:
    QList<QObject*> _podcastList;
    QList<QObject*> _episodeList;
    DatabaseManager* _databaseManager;
    DownloadManager* _downloadManager;
    QTimer* _updateTimer;
    qint64 _updateTimerInterval;

public:
    explicit Kaster(QObject *parent = nullptr);

    void reloadEpisodeList();
    KAboutData getAboutData();
    QList<QObject*> getPodcastList();
    QList<QObject*> getEpisodeList();
    Q_INVOKABLE void addNewPodcast(QString url);
    Q_INVOKABLE void downloadEpisode(EpisodeModel* episode);
    Q_INVOKABLE QList<QObject*> getEpisodeListById(qint64 podcastId);
    Q_INVOKABLE QList<QObject*> getEpisodeListFromDate(QDate date = QDate::currentDate().addDays(-QDate::currentDate().daysInMonth() * 3));
    Q_INVOKABLE QList<QObject*> getDownloadedEpisodesList();
    Q_INVOKABLE void deleteDownloadedEpisode(EpisodeModel* episode);
    Q_INVOKABLE void deleteAllDownloadedEpisodes();
    Q_INVOKABLE void markEpisodeAsPlayed(EpisodeModel* episode);
    Q_INVOKABLE void updatePodcasts();
    Q_INVOKABLE void unsubscribe(qint64 podcastId);
    Q_INVOKABLE void updatePlayedDuration(QString episodeTitle, qint64 playedDuration);

signals:
    QList<QObject*> podcastListChanged(QList<QObject*>);
    QList<QObject*> episodeListChanged(QList<QObject*>);
    QList<QObject*> latestEpisodeListChanged(QList<QObject*>);
    QList<QObject*> downloadedEpisodeListChanged(QList<QObject*>);

public slots:
    void updateTimerTimeout();
    void podcastDownloadFinished();
    void podcastUpdateDownloadFinished();

};


#endif // KASTER_H
