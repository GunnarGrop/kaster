/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef EPISODE_MODEL_H
#define EPISODE_MODEL_H

#include <QObject>
#include <QDate>
#include <QNetworkReply>
#include <QFile>
#include <QDir>
#include <QStandardPaths>

class EpisodeModel : public QObject {
    Q_OBJECT

    Q_PROPERTY(QObject* parentPodcast READ getParentPodcast CONSTANT)
    Q_PROPERTY(qint64 episodeId READ getEpisodeId NOTIFY episodeIdChanged)
    Q_PROPERTY(qint64 podcastId READ getPodcastId NOTIFY podcastIdChanged)
    Q_PROPERTY(QString title READ getTitle NOTIFY titleChanged)
    Q_PROPERTY(QString description READ getDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString url READ getUrl CONSTANT)
    Q_PROPERTY(qint64 length READ getLength NOTIFY lengthChanged)
    Q_PROPERTY(QString duration READ getDuration NOTIFY durationChanged)
    Q_PROPERTY(QDate date READ getDate NOTIFY dateChanged)
    Q_PROPERTY(bool played READ getPlayed WRITE setPlayed NOTIFY playedChanged)
    Q_PROPERTY(QString filePath READ getFilePath NOTIFY filePathChanged)
    Q_PROPERTY(qint64 downloadProgress READ getDownloadProgress NOTIFY downloadProgressChanged)
    Q_PROPERTY(qint64 playedDuration READ getPlayedDuration NOTIFY playedDurationChanged)

private:
    qint64 _episodeId;
    qint64 _podcastId;
    QString _title;
    QString _description;
    QString _url;
    qint64 _length;
    QString _duration;
    QDate _date;
    bool _played;
    QString _filePath;
    qint64 _downloadProgress;
    qint64 _playedDuration;

public:
    explicit EpisodeModel(qint64 episodeId = 0,
			  qint64 podcastId = 0,
			  QString title = "",
			  QString description = "",
			  QString url = "",
			  qint64 length = 0,
			  QString duration = "",
			  QDate date = QDate::currentDate(),
			  bool played = false,
			  QString filePath = "",
			  qint64 playedDuration = 0,
			  QObject *parent = nullptr);

    QObject* getParentPodcast();
    qint64 getEpisodeId();
    qint64 getPodcastId();
    QString getTitle();
    QString getDescription();
    QString getUrl();
    qint64 getLength();
    QString getDuration();
    QDate getDate();
    Q_INVOKABLE QString getDateFormat(QString format);
    bool getPlayed();
    QString getFilePath();
    qint64 getDownloadProgress();
    qint64 getPlayedDuration();

    void setEpisodeId(qint64 episodeId);
    void setPodcastId(qint64 podcastId);
    void setTitle(QString title);
    void setDescription(QString description);
    void setUrl(QString url);
    void setLength(qint64 length);
    void setDuration(QString duration);
    void setDate(QDate date);
    void setPlayed(bool state);
    void setFilePath(QString filePath);
    void setPlayedDuration(qint64 playedDuration);

    Q_INVOKABLE void emitAbortDownload();

signals:
    qint64 episodeIdChanged(qint64);
    qint64 podcastIdChanged(qint64);
    QString titleChanged(QString);
    QString descriptionChanged(QString);
    QString urlChanged(QString);
    qint64 lengthChanged(qint64);
    QString durationChanged(QString);
    QDate dateChanged(QDate);
    bool playedChanged(bool);
    QString filePathChanged(QString);
    qint64 downloadProgressChanged(qint64);
    void abortDownload();
    qint64 playedDurationChanged(qint64);

public slots:
    void downloadReady();
    void downloadProgressSlot(qint64 bytesReceived, qint64 bytesTotal);
};


#endif // EPISODE_MODEL_H
