/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include "MediaPlayer.h"
#include "Kaster.h"
#include "WindowManager.h"

int main(int argc, char *argv[]) {
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

#ifdef Q_OS_ANDROID
    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle(QStringLiteral("Material"));
#else
    QApplication app(argc, argv);
#endif

    KAboutData aboutData("kaster",
			 i18n("Kaster"),
			 "0.1",
			 i18n("A podcast player"),
			 KAboutLicense::GPL_V3,
			 i18n("Copyright 2021 Gunnar Andersson"),
			 QString(),
			 "https://gitlab.com/GunnarGrop/kaster");
    aboutData.addAuthor("Gunnar Andersson", "", "gunnar.grop@protonmail.com");

    KAboutData::setApplicationData(aboutData);
    QCoreApplication::setApplicationName("kaster");

    Kaster kaster;
    qmlRegisterSingletonInstance<Kaster>("org.kaster.backend", 1, 0, "Kaster", &kaster);
    MediaPlayer mediaPlayer;
    qmlRegisterSingletonInstance<MediaPlayer>("org.kaster.backend", 1, 0, "MediaPlayer", &mediaPlayer);
    WindowManager windowManager;
    qmlRegisterSingletonInstance<WindowManager>("org.kaster.backend", 1, 0, "WindowManager", &windowManager);

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/contents/ui/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
