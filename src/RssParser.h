/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef RSS_PARSER_H
#define RSS_PARSER_H

#include <QXmlStreamReader>
#include <QDebug>
#include "PodcastModel.h"
#include "EpisodeModel.h"

class RssParser {

private:
    QXmlStreamReader* _streamReader;

public:
    explicit RssParser(QIODevice *device);

    bool isValidDocument();
    PodcastModel* readPodcastInfo(qint64 newPodcastId, QString rssUrl);
    EpisodeModel* readEpisodeInfo(qint64 newEpisodeId, qint64 newPodcastId);
    bool episodeAvailable(); // Needs to be called after readPodcastinfo()

};


#endif // RSS_PARSER_H
