/*
 * SPDX-FileCopyrightText: Copyright 2021 Gunnar Andersson <gunnar.grop@protonmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "PodcastModel.h"

PodcastModel::PodcastModel(qint64 id,
			   QString title,
			   QString description,
			   QString rssUrl,
			   QString imageUrl,
			   QString homepageUrl,
			   QList<QObject*> episodeList,
			   QObject *parent) : QObject(parent)
{
    _id = id;
    _title = title;
    _description = description;
    _rssUrl = rssUrl;
    _imageUrl = imageUrl;
    _homepageUrl = homepageUrl;
    _episodeList = episodeList;
}

qint64 PodcastModel::getId() { return _id; }
QString PodcastModel::getTitle() { return _title; }
QString PodcastModel::getDescription() { return _description; }
QString PodcastModel::getRssUrl() { return _rssUrl; }
QString PodcastModel::getImageUrl() { return _imageUrl; }
QString PodcastModel::getHomepageUrl() { return _homepageUrl; }
QList<QObject*> PodcastModel::getEpisodeList() { return _episodeList; }

void PodcastModel::setId(qint64 id) {
    _id = id;
    emit idChanged(_id);
}

void PodcastModel::setTitle(QString title) {
    _title = title;
    emit titleChanged(_title);
}

void PodcastModel::setDescription(QString description) {
    _description = description;
    emit descriptionChanged(_description);
}

void PodcastModel::setRssUrl(QString rssUrl) {
    _rssUrl = rssUrl;
    emit rssUrlChanged(_rssUrl);
}

void PodcastModel::setImageUrl(QString imageUrl) {
    _imageUrl = imageUrl;
    emit imageUrlChanged(_imageUrl);
}

void PodcastModel::setHomepageUrl(QString homepageUrl) {
    _homepageUrl = homepageUrl;
    emit homepageUrlChanged(_homepageUrl);
}

void PodcastModel::setEpisodeList(QList<QObject*> episodeList) {
    _episodeList = episodeList;
    emit episodeListChanged(_episodeList);
}

void PodcastModel::appendEpisode(EpisodeModel *episode) {
    _episodeList.append(episode);
    emit episodeListChanged(_episodeList);
}

// SLOTS
void PodcastModel::imageDownloadReady() {
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    QString filePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/downloads";
    QString fileName = reply->url().fileName();

    QDir dir(filePath);
    if (!dir.exists()) {
	dir.mkpath(filePath);
    }

    QFile newFile(filePath + "/" + fileName);
    if (newFile.open(QIODevice::WriteOnly)) {
	newFile.write(reply->readAll());
	newFile.close();
	setImageUrl(filePath + "/" + fileName);
    }
    else {
	qDebug() << "Failed to open new file: " << newFile.errorString();
    }

    reply->deleteLater();
}
